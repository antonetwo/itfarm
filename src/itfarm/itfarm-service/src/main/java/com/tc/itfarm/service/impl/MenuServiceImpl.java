package com.tc.itfarm.service.impl;

import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.api.util.DateUtils;
import com.tc.itfarm.dao.MenuDao;
import com.tc.itfarm.model.Menu;
import com.tc.itfarm.service.MenuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wangdongdong on 2016/8/29.
 */
@Service
public class MenuServiceImpl extends BaseServiceImpl<Menu> implements MenuService {

    @Resource
    private MenuDao menuDao;

    @Override
    protected SingleTableDao getSingleDao() {
        return menuDao;
    }

    @Override
    public List<Menu> selectAll() {
        List<Menu> menus = super.selectAll();
        if (menus.size() < 1) {
            Menu m = new Menu();
            m.setName("首页");
            m.setUrl("/article/list.do");
            m.setRemark("HOME");
            m.setCreateTime(DateUtils.now());
            m.setModifyTime(DateUtils.now());
            menuDao.insert(m);
            menus.add(m);
        }
        return menus;
    }
}
