package com.tc.itfarm.service;

import com.tc.itfarm.model.Menu;

/**
 * Created by wangdongdong on 2016/8/29.
 */
public interface MenuService extends BaseService<Menu> {
}
