/*
 * @Project: itfarm
 * @Author shenke
 * @(#)RedisInterface.java  Created on 2016年6月22日
 * @Copyright (c) 2016 ZDSoft Inc. All rights reserved
 * @Date 2016年6月22日
 */
package net.keshen.cache.redis;
/**
 * @description: TODO
 * @author shenke
 * @version Revision: 1.0 , 
 * @date: 2016年6月22日 下午5:14:03
 */
public abstract interface RedisInterface<T> {

	 T queryData();
}
