<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title> 码农的世界，你不懂 </title>
<link href="${ctx }/css/base.css" rel="stylesheet">
<link href="${ctx }/css/new.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="js/modernizr.js"></script>
<![endif]-->
</head>
<body>
<jsp:include page="../index/header.jsp"/>
<header>
  <div id="logo"><a href="${ctx }/"></a></div>
</header>

<article class="blogs">
  <h1 class="t_nav"><span>您当前的位置：<a href="${ctx }/article/list.do">首页</a>&nbsp;&gt;&nbsp;<a href="#">${item.article.title }</a></h1>
  <div class="index_about">
    <h2 class="c_titile">${item.article.title }</h2>
    <p class="box_c"><span class="d_time">发布时间：${item.lastDate }</span><span>编辑：${item.authorName }</span></p>
    <ul class="infos">
    	${item.article.content }
    </ul>
    <div class="keybq">
    <p><span>关键字词</span>：${item.article.keyword }</p>
    
    </div>
    <div class="ad"> </div>
    <div class="nextinfo">
    	<c:if test="${item.last != null }">
      <p>上一篇：<a href="${ctx }/article/detail.do?id=${item.last.recordId}">${item.last.title }</a></p>
      </c:if>
      <c:if test="${item.next != null }">
      <p>下一篇：<a href="${ctx }/article/detail.do?id=${item.next.recordId}">${item.next.title }</a></p>
      </c:if>
    </div>
    <div class="otherlink">
      <h2>相关文章</h2>
      <ul>
        <li><a href="/news/s/2013-07-25/524.html" title="现在，我相信爱情！">现在，我相信爱情！</a></li>
        <li><a href="/newstalk/mood/2013-07-24/518.html" title="我希望我的爱情是这样的">我希望我的爱情是这样的</a></li>
        <li><a href="/newstalk/mood/2013-07-02/335.html" title="有种情谊，不是爱情，也算不得友情">有种情谊，不是爱情，也算不得友情</a></li>
        <li><a href="/newstalk/mood/2013-07-01/329.html" title="世上最美好的爱情">世上最美好的爱情</a></li>
        <li><a href="/news/read/2013-06-11/213.html" title="爱情没有永远，地老天荒也走不完">爱情没有永远，地老天荒也走不完</a></li>
        <li><a href="/news/s/2013-06-06/24.html" title="爱情的背叛者">爱情的背叛者</a></li>
      </ul>
    </div>
  </div>
  <jsp:include page="../index/right.jsp"></jsp:include>
</article>
<footer>
  <p>Design by DanceSmile <a href="http://www.mycodes.net/" title="源码之家" target="_blank">源码之家</a> <a href="/">网站统计</a></p>
</footer>
<script src="${ctx }/js/silder.js"></script>
</body>
</html>