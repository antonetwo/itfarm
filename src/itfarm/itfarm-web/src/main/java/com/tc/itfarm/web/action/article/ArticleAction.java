package com.tc.itfarm.web.action.article;

import com.tc.itfarm.api.common.Codes;
import com.tc.itfarm.api.enums.QueryArticleEnum;
import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.model.Article;
import com.tc.itfarm.service.ArticleService;
import com.tc.itfarm.service.MenuService;
import com.tc.itfarm.service.SystemConfigService;
import com.tc.itfarm.web.biz.ArticleBiz;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/article")
public class ArticleAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(ArticleAction.class);
	@Resource
	private ArticleService articleService;
	@Resource
	private ArticleBiz articleBiz;
	@Resource
	private SystemConfigService systemConfigService;
	@Resource
	private MenuService menuService;

	@RequestMapping("/list")
	public ModelAndView list(@RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo, HttpServletRequest request) {
		request.getSession().setAttribute("config", systemConfigService.selectAll().get(0));
		pageNo = pageNo==null ? 0 : pageNo;
		ModelAndView mv = new ModelAndView("article/index");
		Page page = new Page(pageNo, Codes.COMMON_PAGE_SIZE);
		PageList<Article> pageList = articleService.selectArticleByPage(page, null, null);
		mv.addObject("articles", articleBiz.getArticleVOs(pageList.getData(), request));
		// 用于图片滑动展示
		mv.addObject("animations", articleBiz.getArticleVOs(articleService.selectArticles(QueryArticleEnum.PAGE_VIEW, 5), request));
		mv.addObject("newArticles", articleBiz.getArticleVOs(articleService.selectArticles(QueryArticleEnum.CREATE_TIME, 8), request));
		mv.addObject("page", pageList.getPage());
		request.getSession().setAttribute("menus", menuService.selectAll());
		request.getSession().setAttribute("menuSelected", 1);
		return mv;
	}

	@RequestMapping("/detail")
	public ModelAndView detail(@RequestParam(value = "id", required = true) Integer id, HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView("article/detail");
		String str = "asdasd";
		mv.addObject("item", articleBiz.getArticleVOById(id));
		mv.addObject("animations", articleBiz.getArticleVOs(articleService.selectArticles(QueryArticleEnum.PAGE_VIEW, 5), request));
		mv.addObject("newArticles", articleBiz.getArticleVOs(articleService.selectArticles(QueryArticleEnum.CREATE_TIME, 5), request));
		return mv;
	}

}
