package com.tc.itfarm.web.task;

import com.tc.itfarm.api.util.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by wangdongdong on 2016/8/9.
 */
public class TaskJob {
    private static final Logger logger = LoggerFactory.getLogger(TaskJob.class);

    public void sayHello() {
        logger.info("定时任务开始........");
        System.out.println("时间:" + DateUtils.now() + "大家好......");
        logger.info("定时任务结束........");
    }
}
